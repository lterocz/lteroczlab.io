# László Teróczki


Phone: + 36 20 4233489   
Email: <teroczki@gmx.net>   
CV: <https://lterocz.gitlab.io/> [[Download as PDF]](https://gitlab.com/lterocz/lterocz.gitlab.io/raw/master/docs/cv_teroczki_laszlo.pdf?inline=false)

---

## Professional Experience

### March 2017 – present: evosoft Hungary Kft.
**Infrastructure Engineer**

* Design and implement AWS Infrastructure for MindSphere (cloud-based IoT operating system from Siemens)   
* Manage the runner/build environment
* Technologies:
> `AWS`, `IaC`, `Terraform`, `Packer`   
> `Linux`, `Docker`, `Image scanner tools` (`aquasec`, `clair`)   
> `Git`, `Gitlab`, `Gitlab CI`   
> `ElasticSearch`, `JFrog Artifactory`, `SonarQube`   


### June 2015 – December 2015: evosoft Hungary Kft.
**DevOps Engineer**

* Responsible for CI/CD pipeline for MindSphere (cloud-based IoT operating system from Siemens)   
* Technologies:
> `TFS`, `Jenkins`, `Gradle`, `ESXi`


### 2004 – March 2017: evosoft Hungary Kft.
**Configuration Manager**

* Administer TFS and TFS build infrastucture
* ClearCase Server administration and user support (Unix/Linux, Windows)
* ClearQuest administration, user support, and workflow development
* Unix/Linux server administration
* Technologies:
> `ClearCase`, `ClearQuest`, `TFS`, `Solaris`, `Linux`, `SharePoint`, `ESXi`, `bash`, `perl`


### 2000 - 2003: Hospnet Kft., Debrecen
**Developer**

* Develop Healthcare application
* Technologies:
> `PowerBuilder`, `Oracle`, `PL/SQL stored procedures`

---

## Education
* 1996-2000: Programmer Mathematician (BSc), Debreceni Egyetem

---
## Languages
* Hungarian: mother tongue
* English: advanced
* German: intermediate

---
## Hobbies
* Sports: running, cycling
